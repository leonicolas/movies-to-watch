import gkeepapi
import json

# Package The Movie DataBase
from tmdbv3api import TMDb
from tmdbv3api import Movie

# Imports locaux
from classes.movie import MyMovie
from utils import downloader

tmdb = TMDb()
keep = gkeepapi.Keep()
movie = Movie()

with open('data.json') as json_file:
    data = json.load(json_file)
    tmdb.api_key = data['tmdbapi']['apikey']
    tmdb.language = data['tmdbapi']['language']
    # tmdb.debug = True
    tmdb.debug = False
    success = keep.login(data['gkeepapi']['email'], data['gkeepapi']['password'])
    notes = list(keep.find(query=data['gkeepapi']['listname']))

def get_raw_movie_list(note_text):
    """Renvoie une liste de chaine de caracteres decrivant chaque ligne de la
    note"""
    return note_text.split('\n')

def get_movie_object(raw_movie):
    """Renvoie un objet Movie pour une chaine de caractere correspondant à une
    ligne de la note"""
    return MyMovie(raw_movie)

def get_object_movie_list(raw_movie_list):
    """Renvoie une liste de Movie pour une liste de chaine de caractere
    decrivant chaque ligne d'une note"""
    return list(map(get_movie_object, raw_movie_list))

if len(notes) == 1 :
    note_films = keep.get(notes[0].id)
    movie_lst = get_object_movie_list(get_raw_movie_list(note_films.text))

for my_movie in movie_lst:
    search = movie.search(my_movie.get_title())
    if not my_movie.get_seen():
        res = search[0]
        print(res.poster_path)
        downloader.download_image(res.poster_path, data['tmdbapi']['base-url'] + res.poster_path, res.title)
        print(res.overview)
        print(res.vote_average)
        print()
