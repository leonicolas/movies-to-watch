import re

class MyMovie:
    """
    """

    def __init__(self, movie_raw):
        self.raw_info = movie_raw
        self.title = self.get_words()
        self.seen = self.get_seen()

    def __str__(self):
        return "Title: " + str(self.title) + ", seen: " + str(self.seen)

    def get_words(self):
        return re.findall(r'\w+', self.raw_info.upper())

    def get_seen(self):
        """Renvoie vrai si le code du premier caractere de la chaine décrivant
        le film est une case cochée (code : 9745)"""
        return str(ord(self.raw_info[:1])) == '9745'

    def get_title(self):
        return ' '.join(self.title)
