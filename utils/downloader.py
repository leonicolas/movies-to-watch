import requests
import shutil
import os.path
import sys

dirname = os.path.dirname(__file__)

def download_image(poster_path, url, name):
    path = os.path.join(dirname, "".join(["..\\images", poster_path.replace('/', '\\')]))
    if not os.path.exists(path):
        r = requests.get(url, stream=True)
        if r.status_code == 200:
            print("[INFO] downloading : {0}".format(name))
            with open(path, 'wb+') as f:
                for chunk in r.iter_content(1024):
                    f.write(chunk)
